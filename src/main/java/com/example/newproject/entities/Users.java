package com.example.newproject.entities;

import com.example.newproject.enums.UserRole;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Users extends BaseEntity {
    @Column(unique = true,length = 128)
    private String username;
    @Column(unique = true)
    private String password;
    @Column(length = 128)
    private String fullName;
    @Enumerated(EnumType.STRING) @Column(length = 64)
    private UserRole role;
    @Column(unique = true)
    private String googleId;
    private String imgUrl;
    private Boolean confirmed;
}
