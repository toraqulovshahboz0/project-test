package com.example.newproject.entities;

import lombok.*;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Feature extends BaseEntity{
    @Embedded
    private LocalizedString name;
    @ManyToOne
    private Product product;
}
