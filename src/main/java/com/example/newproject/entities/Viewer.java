package com.example.newproject.entities;

import lombok.*;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Embeddable
public class Viewer {
    @NotBlank
    private String name;
    @NotBlank
    private String email;
}
