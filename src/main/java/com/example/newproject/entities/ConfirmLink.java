package com.example.newproject.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConfirmLink extends BaseEntity{
    private String uid;
    @ManyToOne
    private Users user;
}
