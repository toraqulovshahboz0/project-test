package com.example.newproject.entities;

import lombok.*;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Feedback extends BaseEntity{
    @ManyToOne
    private Product product;
    private int stars;
    private String advantage;
    private String disadvantage;
    private String comment;
    @Embedded
    private Viewer viewer;
    private Boolean confirmed;
}
