package com.example.newproject.entities;

import lombok.*;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.Embeddable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class LocalizedString {
    private String uz;
    private String ru;
    private String en;

    public String localized(){
        switch (LocaleContextHolder.getLocale().toString()){
            case "ru": return ru;
            case "en": return en;
            default: return uz;
        }
    }
}
