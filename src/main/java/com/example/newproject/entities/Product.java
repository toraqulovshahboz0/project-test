package com.example.newproject.entities;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Product extends BaseEntity {
    private String name;
    @ManyToOne
    private Manufacturer manufacturer;
}
