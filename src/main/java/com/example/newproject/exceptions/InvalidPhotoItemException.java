package com.example.newproject.exceptions;

import com.example.newproject.enums.ErrorCode;

public class InvalidPhotoItemException extends RecommendException {

    @Override
    public ErrorCode errorCode() {
        return ErrorCode.INVALID_PHOTO_ITEM;
    }
}
