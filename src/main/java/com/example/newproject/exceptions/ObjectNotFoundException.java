package com.example.newproject.exceptions;

import com.example.newproject.enums.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;


@AllArgsConstructor
@Data
public class ObjectNotFoundException extends RecommendException {
    private String objectName;
    private String id;


    @Override
    ErrorCode errorCode() {
        return ErrorCode.OBJECT_NOT_FOUND;
    }
}
