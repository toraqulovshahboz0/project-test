package com.example.newproject.exceptions;

import com.example.newproject.dto.ErrorResponse;
import com.example.newproject.enums.ErrorCode;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;

public abstract class RecommendException extends RuntimeException {
    abstract ErrorCode errorCode();

    public ResponseEntity<ErrorResponse> response(MessageSource source, Object... args) {
        return ResponseEntity.badRequest().body(ErrorResponse.of(
                        errorCode().getCode(),
                        source.getMessage(errorCode().name(), args, LocaleContextHolder.getLocale())
                )
        );
    }
}
