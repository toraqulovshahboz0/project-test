package com.example.newproject.exceptions;

public class GeneralApiException extends RuntimeException{
    public GeneralApiException(String message) {
        super(message);
    }
}
