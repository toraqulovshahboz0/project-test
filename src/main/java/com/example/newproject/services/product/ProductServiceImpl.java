package com.example.newproject.services.product;

import com.example.newproject.dto.product.ProductCreatedDto;
import com.example.newproject.dto.product.ProductDto;
import com.example.newproject.dto.product.ViewProduct;
import com.example.newproject.entities.Product;
import com.example.newproject.repositories.FileItemRepository;
import com.example.newproject.repositories.ProductRepository;
import com.example.newproject.services.manufacturer.ManufacturerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final FileItemRepository fileRepository;
    private final ManufacturerService manufacturerService;

    @Override
    public ProductDto add(ProductCreatedDto dto) {
        return ProductDto.toDto(repository.save(
                Product.builder()
                        .name(dto.getName())
                        .manufacturer(manufacturerService.getEntity(dto.getManufacturerId()))
                        .build())
        );
    }

    @Override
    public ProductDto update(Long id, ProductDto dto) {
        Product product = get(id);
        product.setName(dto.getName());
        return ProductDto.toDto(repository.save(product));
    }

    @Override
    public void delete(Long id) {
        repository.delete(get(id));
    }

    @Override
    public ProductDto getOne(Long id) {
        return ProductDto.toDto(get(id));
    }

    @Override
    public List<ProductDto> findAll() {
        List<ProductDto> productDtos = new ArrayList<>();
        repository.findAll().forEach(it -> productDtos.add(ProductDto.toDto(it)));
        return productDtos;
    }

    public Page<ViewProduct> getProductsWithStars(Pageable pageable) {
        return repository.findAllWithStars(pageable);
    }

    @Override
    public ViewProduct getProductWithStars(Long id) {
        return repository.findOneWithStars(id);
    }

    @Override
    public Page<ProductDto> search(String keyword, Pageable pageable) {
        return repository.findAllByNameContainingIgnoreCase(keyword, pageable).map(ProductDto::toDto);
    }


    private Product get(Long id) {
        Optional<Product> byDeletedFalseAndId = repository.findByDeletedFalseAndId(id);
        if (byDeletedFalseAndId.isPresent()) {
            return byDeletedFalseAndId.get();
        } else {
            throw new RuntimeException("Product not found!");
        }

    }
}
