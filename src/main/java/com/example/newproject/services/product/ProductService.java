package com.example.newproject.services.product;

import com.example.newproject.dto.product.ProductCreatedDto;
import com.example.newproject.dto.product.ProductDto;
import com.example.newproject.dto.product.ViewProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {

    ProductDto add(ProductCreatedDto dto);

    ProductDto update(Long id, ProductDto dto);

    void delete(Long id);

    ProductDto getOne(Long id);

    List<ProductDto> findAll();

    Page<ViewProduct> getProductsWithStars(Pageable pageable);
    ViewProduct getProductWithStars(Long id);
    Page<ProductDto> search(String keyword, Pageable pageable);


}
