package com.example.newproject.services.fileItem;

import com.example.newproject.dto.FileItemDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface FileItemService {
    FileItemDto saveFile(MultipartFile file) throws IOException;

    byte[] getFile(String uid) throws IOException;
}
