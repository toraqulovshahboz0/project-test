package com.example.newproject.services;

import com.example.newproject.dto.category.CategoryDto;
import com.example.newproject.dto.category.CategoryForm;
import com.example.newproject.entities.Category;
import com.example.newproject.repositories.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public CategoryDto add(CategoryForm form) {
        Category category = CategoryForm.fromDto(form);
        repository.save(category);
        return CategoryDto.toDto(category);

    }

    @Override
    public CategoryDto update(Long id, CategoryForm form) {
        Category category = get(id);
        category.setName(form.getName());
        repository.save(category);
        return CategoryDto.toDto(category);

    }

    @Override
    public void delete(Long id) {
        repository.delete(get(id));
    }

    @Override
    public CategoryDto getOne(Long id) {
        return CategoryDto.toDto(get(id));
    }


    @Override
    public List<CategoryDto> findAll() {
        return repository.findAll().stream().map(CategoryDto::toDto).collect(Collectors.toList());
    }

    @Override
    public Category getEntity(Long id) {
        return get(id);
    }


    private Category get(Long id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new RuntimeException("Category id not found:" + id);
        });


    }
}



