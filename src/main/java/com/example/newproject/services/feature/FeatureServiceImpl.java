package com.example.newproject.services.feature;

import com.example.newproject.dto.feature.FeatureAddDto;
import com.example.newproject.dto.feature.FeatureDto;
import com.example.newproject.dto.feature.FeatureLocalizedDto;
import com.example.newproject.entities.Feature;
import com.example.newproject.repositories.FeatureRepository;
import com.example.newproject.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FeatureServiceImpl implements FeatureService {
    private final FeatureRepository repository;
    private final ProductRepository productRepository;

    @Override
    public List<FeatureDto> getAll(Long productId) {
        return repository.findByProductIdAndDeletedFalse(productId)
                .stream().map(FeatureDto::toDto).collect(Collectors.toList());
    }

    @Override
    public FeatureLocalizedDto getAllLocalized(Long productId) {
        return FeatureLocalizedDto.toDto(
                repository.findByProductIdAndDeletedFalse(productId),
                productId
        );
    }

    @Override
    public FeatureLocalizedDto add(FeatureAddDto dto) {
        var entities = dto.getFeaturesName()
                .stream()
                .map(featureName -> Feature.builder()
                        .name(featureName)
                        .product(productRepository.findByIdAndDeletedFalse(dto.getProductId()).orElseThrow(() -> new RuntimeException("no such product!")))
                        .build())
                .collect(Collectors.toList());
        repository.saveAll(entities);
        return FeatureLocalizedDto.toDto(
                entities,
                dto.getProductId()
        );
    }

    @Override
    public void delete(Long id) {
        repository.deletedTrue(id);
    }
}