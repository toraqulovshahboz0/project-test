package com.example.newproject.services.feature;

import com.example.newproject.dto.feature.FeatureAddDto;
import com.example.newproject.dto.feature.FeatureDto;
import com.example.newproject.dto.feature.FeatureLocalizedDto;

import java.util.List;

public interface FeatureService {

    List<FeatureDto> getAll(Long productId);

    FeatureLocalizedDto getAllLocalized(Long productId);

    FeatureLocalizedDto add(FeatureAddDto dto) ;

    void delete(Long id);
}
