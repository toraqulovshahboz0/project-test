package com.example.newproject.services.users;


import com.example.newproject.dto.UsersDto;

import java.security.GeneralSecurityException;
import java.util.List;

public interface UsersService {

    UsersDto add(UsersDto dto) ;

   // UsersDto changePassword(Long id, UsersDto dto);

    void delete(Long id);

    UsersDto getOne(Long id);

    List<UsersDto> findAll();


    Boolean confirmation(String uuid);

}
