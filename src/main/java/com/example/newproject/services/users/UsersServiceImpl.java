package com.example.newproject.services.users;

import com.example.newproject.dto.UsersDto;
import com.example.newproject.entities.Users;
import com.example.newproject.exceptions.GeneralApiException;
import com.example.newproject.exceptions.ObjectNotFoundException;
import com.example.newproject.repositories.ConfirmLinkRepository;
import com.example.newproject.repositories.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {
    private final ConfirmLinkRepository linkRepository;
    private final UsersRepository repository;
//      private final BCryptPasswordEncoder encoder;

    @Override
    public UsersDto add(UsersDto dto) {
        if (repository.existsByUsername(dto.getUserName()))
            throw new GeneralApiException("Username already used!");
        var user = repository.save(Users.builder()
                .username(dto.getUserName())
                .password(dto.getPassword())
                .fullName(dto.getFullName())
                .role(dto.getRole())
                .confirmed(false)
                .build());
        return UsersDto.toDto(user);
    }

//    @Override
//    public UsersDto
//    changePassword(Long id, UsersDto dto) {
//        Users user = get(id);
//        user.setPassword(encoder.encode(dto.getPassword()));
//        return UsersDto.toDto(repository.save(user));
//    }

    @Override
    public void delete(Long id) {
        Users user = get(id);
        user.setDeleted(true);
        repository.save(user);
    }

    @Override
    public UsersDto getOne(Long id) {
        return UsersDto.toDto(get(id));
    }

    @Override
    public List<UsersDto> findAll() {
        return repository.findAll().stream().map(UsersDto::toDto).collect(Collectors.toList());
    }


    @Override
    public Boolean confirmation(String uuid) {
        var link = linkRepository.findByUid(uuid).orElseThrow(() -> new GeneralApiException("UUID not found!"));
        Users user = link.getUser();
        user.setConfirmed(true);
        repository.save(user);
        linkRepository.delete(link);
        return true;
    }


    private Users get(Long id) {
        Optional<Users> optionalUser = repository.findByDeletedFalseAndId(id);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new ObjectNotFoundException("User", id.toString());
        }
    }


}
