package com.example.newproject.services;


import com.example.newproject.dto.category.CategoryDto;
import com.example.newproject.dto.category.CategoryForm;
import com.example.newproject.entities.Category;

import java.util.List;

public interface CategoryService {

    CategoryDto add(CategoryForm dto);

    CategoryDto update(Long id, CategoryForm form);

    void delete(Long id);

    CategoryDto getOne(Long id);

    List<CategoryDto> findAll();

    Category getEntity(Long id);
}
