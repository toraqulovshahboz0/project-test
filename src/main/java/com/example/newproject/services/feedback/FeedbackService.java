package com.example.newproject.services.feedback;

import com.example.newproject.dto.FeedbackDto;
import com.example.newproject.dto.FeedbackForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface FeedbackService {

    FeedbackDto add(FeedbackForm form);

    FeedbackDto update(Long id, FeedbackForm form);

    void trash(Long id);

    FeedbackDto getOne(Long id);

    Page<FeedbackDto> findAllUnconfirmed(Pageable pageable);

    List<FeedbackDto> findAllConfirmed(Long productId);

//    FeedbackDto confirm(Long id, boolean confirmed);
}
