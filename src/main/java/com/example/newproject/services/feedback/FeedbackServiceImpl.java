package com.example.newproject.services.feedback;

import com.example.newproject.dto.FeedbackDto;
import com.example.newproject.dto.FeedbackForm;
import com.example.newproject.entities.Feedback;
import com.example.newproject.entities.Viewer;
import com.example.newproject.repositories.FeedbackRepository;
import com.example.newproject.repositories.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository repository;
    private final ProductRepository productRepository;

    //    private final SimpMessagingTemplate template;
    @Override
    public FeedbackDto getOne(Long id) {
        return FeedbackDto.toDto(get(id));
    }

    @Override
    public FeedbackDto add(FeedbackForm form) {
        return FeedbackDto.toDto(repository.save(fill(new Feedback(), form)));
    }

    @Override
    public FeedbackDto update(Long id, FeedbackForm form) {
        return FeedbackDto.toDto(repository.save(fill(get(id), form)));
    }

    @Override
    public void trash(Long id) {
        repository.deletedTrue(id);
    }

    @Override
    public Page<FeedbackDto> findAllUnconfirmed(Pageable pageable) {
        return repository.findAllByConfirmedAndDeletedFalse(null, pageable)
                .map(FeedbackDto::toDto);
    }

    @Override
    public List<FeedbackDto> findAllConfirmed(Long productId) {
        return repository.findAllByProductIdAndConfirmedAndDeletedFalse(productId, true)
                .stream()
                .map(FeedbackDto::toDto)
                .collect(Collectors.toList());
    }

//   @Override
//    public FeedbackDto confirm(Long id, boolean confirmed) {
//        Feedback feedback = get(id);
//        feedback.setConfirmed(confirmed);
//        repository.save(feedback);
//        template.convertAndSendToUser(feedback.getCreatedBy(),"/directional/confirm-feedback/", feedback.getId()+" shu feedbackingiz moderatsiyadan o'tdi!");
//        return FeedbackDto.toDto(feedback);
//    }

    private Feedback fill(Feedback feedback, FeedbackForm form) {
        Viewer viewer = Viewer.of(form.getViewer().getName(), form.getViewer().getEmail());
        feedback.setProduct(productRepository.findByIdAndDeletedFalse(form.getProductId()).orElseThrow(() -> new RuntimeException("no such product!")));
        feedback.setAdvantage(form.getAdvantages());
        feedback.setDisadvantage(form.getDisadvantages());
        feedback.setComment(form.getComment());
        feedback.setStars(form.getStars());
        feedback.setViewer(viewer);
        return feedback;
    }

    private Feedback get(Long id) {
        return repository.findByIdAndDeletedFalse(id).orElseThrow(() -> new RuntimeException("no such feedback!"));
    }
}