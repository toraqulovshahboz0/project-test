package com.example.newproject.services.manufacturer;

import com.example.newproject.dto.manifacture.ManufacturerDto;
import com.example.newproject.dto.manifacture.ManufacturerForm;
import com.example.newproject.dto.manifacture.ManufacturerView;
import com.example.newproject.entities.Manufacturer;
import com.example.newproject.repositories.ManufacturerRepository;
import com.example.newproject.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ManufacturerServiceImpl implements ManufacturerService {

    private final ManufacturerRepository repository;
    private final CategoryService categoryService;

    @Override
    public ManufacturerDto add(ManufacturerForm form) {
        return ManufacturerDto.toDto(
                repository.save(
                        ManufacturerForm.toEntity(form, categoryService.getEntity(form.getCategoryId()))
                )
        );
    }

    @Override
    public ManufacturerDto update(ManufacturerForm form, Long id) {
        Manufacturer manufacturer = get(id);
        manufacturer.setName(form.getName());
        repository.save(manufacturer);
        return ManufacturerDto.toDto(manufacturer);
    }

    @Override
    public void delete(Long id) {
        repository.delete(get(id));
    }

    @Override
    public ManufacturerDto getOne(Long id) {
        return ManufacturerDto.toDto(get(id));
    }

    @Override
    public Manufacturer getEntity(Long id) {
        return get(id);
    }

    @Override
    public List<ManufacturerView> findAll() {
        return repository.findAllWithProductCount();
    }


    private Manufacturer get(Long id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new RuntimeException("Manufacturer id is not" + id);
        });

    }
}
