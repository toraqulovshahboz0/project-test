package com.example.newproject.services.manufacturer;


import com.example.newproject.dto.manifacture.ManufacturerDto;
import com.example.newproject.dto.manifacture.ManufacturerForm;
import com.example.newproject.dto.manifacture.ManufacturerView;
import com.example.newproject.entities.Manufacturer;

import java.util.List;

public interface ManufacturerService {
    ManufacturerDto add(ManufacturerForm form);
    ManufacturerDto update(ManufacturerForm form, Long id);
    void delete(Long id);
    ManufacturerDto getOne(Long id);
    Manufacturer getEntity(Long id);
    List<ManufacturerView> findAll();
}

