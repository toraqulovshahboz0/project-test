package com.example.newproject.controller;

import com.example.newproject.dto.manifacture.ManufacturerDto;
import com.example.newproject.dto.manifacture.ManufacturerForm;
import com.example.newproject.dto.manifacture.ManufacturerView;
import com.example.newproject.services.manufacturer.ManufacturerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/manufacturer")
@RequiredArgsConstructor
public class ManufacturerController {

    private final ManufacturerService service;

    @PostMapping
    public ManufacturerDto add(@RequestBody ManufacturerForm form) {
        return service.add(form);
    }

    @GetMapping
    public List<ManufacturerView> findAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public ManufacturerDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @PutMapping("/{id}")
    public ManufacturerDto update(@PathVariable Long id, @RequestBody ManufacturerForm form) {
        return service.update(form, id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
