package com.example.newproject.controller;

import com.example.newproject.dto.FeedbackDto;
import com.example.newproject.dto.FeedbackForm;
import com.example.newproject.services.feedback.FeedbackService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/v1/feedback")
@RequiredArgsConstructor
public class FeedbackController {

    private final FeedbackService service;

    @PostMapping
    FeedbackDto add(@RequestBody @Valid FeedbackForm form){
        return service.add(form);
    }

    @PutMapping("/{id}")
    FeedbackDto update(@PathVariable Long id,@RequestBody FeedbackForm form){
        return service.update(id, form);
    }

    @DeleteMapping("/{id}")
    void trash(@PathVariable Long id){
        service.trash(id);
    }

    @GetMapping("/get/{id}")
    FeedbackDto getOne(@PathVariable Long id){
        return service.getOne(id);
    }

    @GetMapping("/unconfirmed")
    Page<FeedbackDto> findAllUnconfirmed(Pageable pageable){
        return service.findAllUnconfirmed(pageable);
    }

//    @PutMapping("/confirm/{id}")
//    FeedbackDto confirm(@PathVariable Long id, @RequestParam boolean confirmed){
//        return service.confirm(id, confirmed);
//    }

    @GetMapping("/confirmed/{productId}")
    List<FeedbackDto> findAllConfirmed(@PathVariable Long productId){
        return service.findAllConfirmed(productId);
    }

}