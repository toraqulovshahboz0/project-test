package com.example.newproject.controller;

import com.example.newproject.dto.FileItemDto;
import com.example.newproject.services.fileItem.FileItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/file")
@RequiredArgsConstructor
public class FileItemController {

    private final FileItemService service;

    @GetMapping(value = "/{uid}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getFile(@PathVariable String uid) throws IOException {
        return service.getFile(uid);
    }

    @PostMapping
    public FileItemDto saveFile(@RequestParam MultipartFile file) throws IOException {
        return service.saveFile(file);
    }
}
