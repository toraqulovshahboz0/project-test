package com.example.newproject.controller;

import com.example.newproject.dto.UsersDto;
import com.example.newproject.services.users.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/users")
public class UsersController {
    private final UsersService service;

    @PostMapping("/registration")
    public UsersDto registration(@RequestBody UsersDto dto) throws MessagingException {
        return service.add(dto);
    }

    @GetMapping("/{id}")
    public UsersDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @GetMapping
    public List<UsersDto> findAll() {
        return service.findAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }


    @GetMapping("/confirmation/{uid}")
    public Boolean confirmation(@PathVariable String uid) {
        return service.confirmation(uid);
    }


}
