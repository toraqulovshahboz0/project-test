package com.example.newproject.controller;


import com.example.newproject.dto.ErrorResponse;
import com.example.newproject.exceptions.InvalidPhotoItemException;
import com.example.newproject.exceptions.ObjectNotFoundException;
import com.example.newproject.exceptions.RecommendException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@RequiredArgsConstructor
@ControllerAdvice
public class HandleExceptions {

    private final MessageSource source;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleException(MethodArgumentNotValidException ex) {
        return ResponseEntity.badRequest().body(ErrorResponse.of(
                1111,
                ex.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(java.util.stream.Collectors.joining(", "))));
    }

    @ExceptionHandler(RecommendException.class)
    public ResponseEntity<ErrorResponse> handleException(RecommendException ex) {
        if (ex instanceof InvalidPhotoItemException) {
            InvalidPhotoItemException exception = (InvalidPhotoItemException) ex;
            return exception.response(source);
        }
        if (ex instanceof ObjectNotFoundException) {
            ObjectNotFoundException exception = (ObjectNotFoundException) ex;
            return exception.response(source, exception.getObjectName(), exception.getId());
        }


        return ResponseEntity.badRequest().body(ErrorResponse.of(
                111,
                ex.getMessage())
        );
    }
}
