package com.example.newproject.controller;

import com.example.newproject.dto.feature.FeatureAddDto;
import com.example.newproject.dto.feature.FeatureDto;
import com.example.newproject.dto.feature.FeatureLocalizedDto;
import com.example.newproject.services.feature.FeatureService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/feature")
@RequiredArgsConstructor
public class FeatureController {

    private final FeatureService service;

    @GetMapping("/{productId}")
    List<FeatureDto> getAll(@PathVariable Long productId){
        return service.getAll(productId);
    }

    @GetMapping("/full/{id}")
    FeatureLocalizedDto getAllLocalized(@PathVariable Long id){
        return service.getAllLocalized(id);
    }

    @PostMapping
    FeatureLocalizedDto add(@RequestBody FeatureAddDto dto){
        return service.add(dto);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id){
        service.delete(id);
    }
}
