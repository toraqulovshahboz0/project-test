package com.example.newproject.controller;

import com.example.newproject.dto.product.ProductCreatedDto;
import com.example.newproject.dto.product.ProductDto;
import com.example.newproject.dto.product.ViewProduct;
import com.example.newproject.services.product.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService service;

    @PostMapping
    public ProductDto add(@RequestBody @Valid ProductCreatedDto dto) {
        return service.add(dto);
    }

    @GetMapping
    public List<ProductDto> findAll() {
        return service.findAll();
    }

    @GetMapping("/view")
    public Page<ViewProduct> findAllWithStars(Pageable pageable) {
        return service.getProductsWithStars(pageable);
    }

    @GetMapping("/view/{id}")
    public ViewProduct findOneViewWithStars(@PathVariable Long id) {
        return service.getProductWithStars(id);
    }

    @GetMapping("/search")
    public Page<ProductDto> search(String keyword, Pageable pageable) {
        return service.search(keyword, pageable);
    }

    @GetMapping("/{id}")
    public ProductDto getOne(@PathVariable Long id) {
        return service.getOne(id);
    }

    @PutMapping("/{id}")
    public ProductDto update(@PathVariable Long id, @RequestBody ProductDto dto) {
        return service.update(id, dto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}