package com.example.newproject.dto;

import com.example.newproject.entities.BaseEntity;
import com.example.newproject.entities.Users;
import com.example.newproject.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsersDto extends BaseEntity {
    private Long id;
    private String fullName;
    private String userName;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private UserRole role;
    private Long confirmKey;

    public static UsersDto toDto(Users save, Long confirmKey){
        UsersDto dto=new UsersDto();
        dto.setId(save.getId());
        dto.setPassword(save.getPassword());
        dto.setFullName(save.getFullName());
        dto.setUserName(save.getUsername());
        dto.setRole(save.getRole());
        dto.setConfirmKey(confirmKey);
        return dto;
    }
    public static UsersDto toDto(Users save){
        UsersDto dto=new UsersDto();
        dto.setId(save.getId());
        dto.setPassword(save.getPassword());
        dto.setFullName(save.getFullName());
        dto.setUserName(save.getUsername());
        dto.setRole(save.getRole());
        return dto;
    }
}
