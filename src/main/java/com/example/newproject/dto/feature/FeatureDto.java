package com.example.newproject.dto.feature;

import com.example.newproject.entities.Feature;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeatureDto {
    private String feature;
    private Long productId;

    public static FeatureDto toDto(Feature feature){
        FeatureDto dto = new FeatureDto();
        Long id = feature.getProduct().getId();
        dto.setProductId(id);
        dto.setFeature(feature.getName().localized());
        return dto;
    }
}
