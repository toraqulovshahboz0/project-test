package com.example.newproject.dto.feature;

import com.example.newproject.entities.Feature;
import com.example.newproject.entities.LocalizedString;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class FeatureLocalizedDto {
    private List<LocalizedString> featureName;
    private Long productId;

    public static FeatureLocalizedDto toDto(List<Feature> feature, Long productId){
        return FeatureLocalizedDto.of(feature.stream().map(Feature::getName).collect(Collectors.toList()), productId);
    }
}
