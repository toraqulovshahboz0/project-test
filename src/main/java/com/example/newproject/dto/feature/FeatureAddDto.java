package com.example.newproject.dto.feature;

import com.example.newproject.entities.LocalizedString;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeatureAddDto {
    private List<LocalizedString> featuresName;
    private Long productId;
}