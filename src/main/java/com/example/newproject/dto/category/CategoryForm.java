package com.example.newproject.dto.category;

import com.example.newproject.entities.Category;
import com.example.newproject.entities.LocalizedString;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryForm {
    private Long id;
    private LocalizedString name;


    public static Category fromDto(CategoryForm dto) {
        return Category.of(dto.getName());
    }

    public static CategoryForm toForm(Category c) {
        return new CategoryForm(c.getId(), c.getName());
    }
}
