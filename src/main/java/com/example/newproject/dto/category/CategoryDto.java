package com.example.newproject.dto.category;

import com.example.newproject.entities.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {
    private Long id;
    private String name;


    public static CategoryDto toDto(Category c) {
        return new CategoryDto(c.getId(), c.getName().localized());
    }
}
