package com.example.newproject.dto.manifacture;

public interface ManufacturerView {
    Long getId();
    String getName();
    Integer getProducts();
}
