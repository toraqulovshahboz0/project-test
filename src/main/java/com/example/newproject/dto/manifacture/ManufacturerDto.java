package com.example.newproject.dto.manifacture;

import com.example.newproject.entities.Manufacturer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManufacturerDto {
    private Long id;
    private String name;

    public static ManufacturerDto toDto(Manufacturer manufacturer) {
        return new  ManufacturerDto(manufacturer.getId(),manufacturer.getName());
    }

}
