package com.example.newproject.dto.manifacture;

import com.example.newproject.entities.Category;
import com.example.newproject.entities.Manufacturer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManufacturerForm {
    private String name;
    private Long categoryId;

    public static Manufacturer toEntity(ManufacturerForm form, Category category) {
        return Manufacturer.of(form.getName(), category);
    }
}