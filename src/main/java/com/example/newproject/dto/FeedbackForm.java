package com.example.newproject.dto;

import com.example.newproject.entities.Viewer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackForm {
    @NotNull
    private Long productId;
    @NotNull
    @Positive
    @Max(value = 5,  message = "Max value for starts is 5")
    private int stars;
    @NotBlank
    private String advantages;
    @NotBlank
    private String disadvantages;
    @NotBlank
    private String comment;
    @NotNull
    private Viewer viewer;
}