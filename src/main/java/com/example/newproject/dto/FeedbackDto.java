package com.example.newproject.dto;

import com.example.newproject.entities.Feedback;
import com.example.newproject.entities.Viewer;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackDto {
    private Long id;
    private Long productId;
    private int stars;
    private String advantages;
    private String disadvantages;
    private String comment;
    private Viewer viewer;
    private Long createdDate;

    public static FeedbackDto toDto(Feedback feedback) {
        return FeedbackDto.builder()
                .id(feedback.getId())
                .productId(feedback.getProduct().getId())
                .stars(feedback.getStars())
                .advantages(feedback.getAdvantage())
                .disadvantages(feedback.getDisadvantage())
                .comment(feedback.getComment())
                .viewer(feedback.getViewer())
                .createdDate(feedback.getCreated().getEpochSecond())
                .build();
    }
}
