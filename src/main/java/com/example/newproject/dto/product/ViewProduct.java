package com.example.newproject.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface ViewProduct {
    Long getId();

    Integer getFeedbacks();

    String getName();

    String getPhoto();

    String getManufacturer();

    Double getStars();
}
