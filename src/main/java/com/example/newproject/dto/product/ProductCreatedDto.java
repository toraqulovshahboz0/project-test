package com.example.newproject.dto.product;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ProductCreatedDto {
    @NotBlank
    private String name;
    private Long manufacturerId;
}
