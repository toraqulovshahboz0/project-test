package com.example.newproject.enums;

public enum UserRole {
    ADMIN, MODERATOR, CLIENT
}
