package com.example.newproject.enums;

import lombok.Getter;

@Getter
public enum ErrorCode {
    INVALID_PHOTO_ITEM(1200),
    OBJECT_NOT_FOUND(1200),
    ;

    private int code;

    ErrorCode(int code) {
        this.code = code;
    }
}
