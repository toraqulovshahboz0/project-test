package com.example.newproject.repositories;

import com.example.newproject.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface UsersRepository extends JpaRepository<Users, Long> {
    Optional<Users> findByUsernameAndDeletedFalse(String username);
    Boolean existsByUsername(String username);
    Optional<Users> findByDeletedFalseAndId(Long id);
}
