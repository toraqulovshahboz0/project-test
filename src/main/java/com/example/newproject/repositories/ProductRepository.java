package com.example.newproject.repositories;

import com.example.newproject.dto.product.ViewProduct;
import com.example.newproject.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByDeletedFalseAndId(Long id);
    Optional<Product> findByIdAndDeletedFalse(Long id);
    @Query(value = "select p.id, p.name as name, m.name as manufacturer,  COALESCE(sum(f.stars) / cast(count(f.id) as decimal), 0) as stars\n" +
            "from product p\n" +
            "left join feedback f on p.id = f.product_id\n" +
            "left join manufacturer m on m.id = p.manufacturer_id\n" +
            "group by p.id, m.name", nativeQuery = true)
    Page<ViewProduct> findAllWithStars(Pageable pageable);

    @Query(value = "select count(f.id) as feedbacks, p.id, p.name as name, m.name as manufacturer,  COALESCE(sum(f.stars) / cast(count(f.id) as decimal), 0) as stars\n" +
            "from product p\n" +
            "left join feedback f on p.id = f.product_id\n" +
            "left join manufacturer m on m.id = p.manufacturer_id where p.id = ?1 " +
            "group by p.id, m.name", nativeQuery = true)
    ViewProduct findOneWithStars(Long productId);

    Page<Product> findAllByNameContainingIgnoreCase(String q,Pageable pageable);
}
