package com.example.newproject.repositories;

import com.example.newproject.entities.Feature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;


public interface FeatureRepository extends JpaRepository<Feature, Long> {
    List<Feature> findByProductIdAndDeletedFalse(Long productId);
    Optional<Feature> findByIdAndDeletedFalse(Long id);
    Set<Feature> findAllByDeletedFalse();

    @Modifying
    @Query(value = "update feature set deleted = true where id = ?1", nativeQuery = true)
    void deletedTrue(Long id);
}
