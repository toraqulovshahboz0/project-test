package com.example.newproject.repositories;


import com.example.newproject.entities.FileItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface FileItemRepository extends JpaRepository<FileItem, Long> {

    Optional<FileItem> findByUidAndDeletedFalse(String uid);
    FileItem getByUid(String uid);

    Boolean existsByUid(String uid);
}
