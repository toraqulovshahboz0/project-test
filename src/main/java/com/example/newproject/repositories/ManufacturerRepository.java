package com.example.newproject.repositories;

import com.example.newproject.dto.manifacture.ManufacturerView;
import com.example.newproject.entities.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {

    @Query(value = "select m.id, m.name, count(p.id) as products\n" +
            "from manufacturer m\n" +
            "         left join product p on p.manufacturer_id = m.id\n" +
            "group by m.id, m.name", nativeQuery = true)
    List<ManufacturerView> findAllWithProductCount();
}
