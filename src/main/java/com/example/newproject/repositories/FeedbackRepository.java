package com.example.newproject.repositories;

import com.example.newproject.entities.Feedback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;


public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
    Optional<Feedback> findByIdAndDeletedFalse(Long id);

    Set<Feedback> findByProductIdAndConfirmedTrueAndDeletedFalse(Long productId);

    List<Feedback> findAllByConfirmedAndDeletedFalse(Boolean confirmed);
    Page<Feedback> findAllByConfirmedAndDeletedFalse(Boolean confirmed, Pageable pageable);
    List<Feedback> findAllByProductIdAndConfirmedAndDeletedFalse(Long productId, Boolean confirmed);
    @Modifying
    @Query(value = "update feedback set deleted = true where id = ?1", nativeQuery = true)
    void deletedTrue(Long id);

}
