package com.example.newproject.repositories;

import com.example.newproject.entities.ConfirmLink;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ConfirmLinkRepository extends JpaRepository<ConfirmLink, Long> {
    Optional<ConfirmLink> findByUid(String uid);
    Optional<ConfirmLink> findByIdAndUid(Long id, String uid);
}
